#include "Visitor.h"
#include <iostream>

using namespace std;

Visitor::Visitor()
{
    //ctor
}

Visitor::~Visitor()
{
    //dtor
}


void Visitor::newVisitor(){


    int iid, iage;
    string iname, irelPat;
    cout << "Digite o numero de identidade do visitante" << endl;
    cin >> iid;
    Setid(iid);

    cout << "Digite a idade do visitante" << endl;
    cin >> iage;
    Setage(iage);

    cout << "Digite o nome do visitante" << endl;
    cin >> iname;
    Setname(iname);

    cout << "Digite o nome do paciente relacionado ao visitante" << endl;
    cin >> irelPat;
    SetrelatedPatient(irelPat);



}



void Visitor::printVisitor(){
    cout << endl <<"# de ID: " << Getid() << endl;
    cout << "Idade: " << Getage() << endl;
    cout << "Nome: " << Getname() << endl;
    cout << "Relacionado ao Paciente: " << GetrelatedPatient() << endl;

}
