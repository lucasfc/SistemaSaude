#ifndef PERSON_H
#define PERSON_H
#include <string>


using namespace std;

class Person
{
    public:
        Person();
        virtual ~Person();

        int Getid() { return id; }
        void Setid(int val) { id = val; }
        int Getage() { return age; }
        void Setage(int val) { age = val; }
        string Getname() { return name; }
        void Setname(string val) { name = val; }

    protected:

    private:
        int id;
        int age;
        string name;
};

#endif // PERSON_H
