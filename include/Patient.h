#ifndef PATIENT_H
#define PATIENT_H

#include <Person.h>


class Patient : public Person
{
    public:
        Patient();
        virtual ~Patient();
        void newPatient();
        void printPatient();
        string Getdiag() { return diag; }
        void Setdiag(string val) { diag = val; }
        string Getdoctor() { return doctor; }
        void Setdoctor(string val) { doctor = val; }

    protected:

    private:
        string diag;
        string doctor;
};

#endif // PATIENT_H
