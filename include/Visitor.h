#ifndef VISITOR_H
#define VISITOR_H

#include <Person.h>


class Visitor : public Person
{
    public:
        Visitor();
        virtual ~Visitor();
        void newVisitor();
        void printVisitor();
        string GetrelatedPatient() { return relatedPatient; }
        void SetrelatedPatient(string val) { relatedPatient = val; }

    protected:

    private:
        string relatedPatient;
};

#endif // VISITOR_H
