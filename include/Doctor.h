#ifndef DOCTOR_H
#define DOCTOR_H
#include "Person.h"

class Doctor : public Person
{
    public:
        Doctor();
        virtual ~Doctor();
        void newDoctor();
        void printDoctor();
        string Getdept() { return dept; }
        void Setdept(string val) { dept = val; }

    protected:

    private:
        string dept;
};

#endif // DOCTOR_H
