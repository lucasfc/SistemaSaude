#include <iostream>
#include <string>
#include <Person.h>
#include <Doctor.h>
#include <Patient.h>
#include <Visitor.h>




using namespace std;

int main()
{
	char ch;
	int num;
	cout.setf(ios::fixed|ios::showpoint);
	Doctor doc1;
    Patient pat1;
	Visitor vis1;
	do
	{
	cout<<"\n\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";
	cout<<"\n1.REGISTRAR NOVO MEDICO";
	cout<<"\n2.REGISTRAR NOVO PACIENTE";
	cout<<"\n3.REGISTRAR NOVO VISITANTE";
    cout<<"\n4.EXIBIR MEDICOS";
    cout<<"\n5.EXIBIR PACIENTES";
    cout<<"\n6.EXIBIR VISITANTES";
    cout<<"\n7.SAIR";
	cout<<"\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";
	cout<<"\nEscolha uma opcao (1-7): ";
	cin>>ch;
	switch(ch)
	{


	case '1':{

	doc1.newDoctor();
    break;


	}
	case '2':{

	pat1.newPatient();
    break;


	}

	case '3':{

	vis1.newVisitor();
    break;


	}

	case '4':{
	doc1.printDoctor();

    break;

	}

	case '5':{
	pat1.printPatient();


    break;
	}
	case '6':{
	vis1.printVisitor();


    break;
	}


	default:	cout<<"\a";

    }
	}while(ch!='7');

	return 0;
}
